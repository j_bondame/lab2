/*
    file:   lab2.c
    author: Teran Bukenberger
    date:   2023-10-02
*/

#include <stdio.h>
#include "/home/joshbond/INFO1216/lab2/lib/const.h"

int main() {
    // Remove the two variables below


    // Display hello to the student
    for ( int idx = 1; idx <= COUNT; ++idx ) {
        printf("Hello, %s!\n", USER);
    }

    // Notify the student that they need to set their variables
    if ( COUNT == 0 && USER == NULL ) {
        printf("COUNT and USER not set!\n");
        return 1;
    }

    // End of program
    return 0;
}

