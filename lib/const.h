/*
    file:   const.h
    author: Teran Bukenberger
    date:   2023-10-02
*/
#ifndef _CONST_H_

// Declare the COUNT variable below (on line 9)
int COUNT = 5;

// Declare the USER variable below (on line 12)
char* USER = "joshbond";

#endif // _CONST_H_
